const fortunes = [
    "Beware of rogue semicolons in your code today.",
    "A mysterious bug will lead you to a new understanding.",
    "Your rubber duck will offer sage advice this afternoon.",
    "You will encounter an array of hope in an unexpected place.",
    "A function in your future will return more than you expect.",
    "Refactoring your life might bring unexpected clarity.",
    "Today is a good day for a backup.",
    "Beware, a Git merge conflict lurks in near future, tread carefully."
];

// Function to display a random fortune
function revealFortune() {
    const fortuneDisplay = document.getElementById('fortune');
    const randomIndex = Math.floor(Math.random() * fortunes.length);
    fortuneDisplay.textContent = fortunes[randomIndex];
}

// Adding event listener to the button
document.getElementById('fortuneButton').addEventListener('click', revealFortune);
